## Setup repo
- Fork repo on solar-10.wpi.edu: http://solar-10.wpi.edu/rmaiudi/Historical-Society-IQP

```shell
# Clones the repo
$ git clone git@solar-10.wpi.edu:{username}/Historical-Society-IQP.git
# Sets the upstream url to the main repo
$ git remote add upstream git@solar-10.wpi.edu:rmaiudi/Historical-Society-IQP.git
```

## Keep your fork in sync:
```shell
# Get changes from master repo
$ git fetch upstream
# Switch to local master branch
$ git checkout master
# Merge changes from master repo with local master branch
$ git rebase upstream/master
```

## How to work on new code
```shell
# Start a new branch
$ git checkout -b {name-of-new-branch <--- should be descriptive of what you are doing}
# Now you can do you work as you please.
```

## Saving your work
```shell
# Add changed files to staging area
$ git add {list of files you changed}  OR  $ git add -A  // Grabs all the changed files
# Commit changes 
$ git commit -m "{Commit message. What did you change?}"
```
